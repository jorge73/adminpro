import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

@Component({
  selector: 'app-increment',
  templateUrl: './increment.component.html',
  styles: []
})
export class IncrementComponent implements OnInit {

  // @Input( 'valor' ) progress = 50; Renombrar valor
  @Input() progress = 50;
  @Input() btnClass: string = 'btn-primary';

  @Output() changeValue: EventEmitter<number> = new EventEmitter();

  ngOnInit(): void {
    this.btnClass = `btn ${this.btnClass}`;
  }

  cambiarValor(valor: number) {
    if (this.progress >= 100 && valor >= 0) {
      this.changeValue.emit(100);
      return this.progress = 100;
    }

    if (this.progress <= 0 && valor < 0) {
      this.changeValue.emit(0);
      return this.progress = 0;
    }
    this.progress = this.progress + valor;
    this.changeValue.emit(this.progress);
  }

  onChange(newValor: number) {

    if ( newValor >= 100) {
      this.progress = 100;
    } else if (newValor <= 0) {
      this.progress = 0;
    } else {
      this.progress = newValor;
    }
    this.changeValue.emit(this.progress);
  }

}
