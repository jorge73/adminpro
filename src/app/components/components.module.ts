import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IncrementComponent } from './increment/increment.component';
import { FormsModule } from '@angular/forms';
import { GraficaComponent } from './grafica/grafica.component';
import { ChartsModule } from 'ng2-charts';



@NgModule({
  declarations: [IncrementComponent, GraficaComponent],
  imports: [
    CommonModule,
    FormsModule,
    ChartsModule
  ],
  exports: [
    IncrementComponent,
    GraficaComponent
  ]
})
export class ComponentsModule { }
