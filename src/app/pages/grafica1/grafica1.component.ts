import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-grafica1',
  templateUrl: './grafica1.component.html',
  styles: []
})
export class Grafica1Component implements OnInit {
  public labels1 = ['Download Sales', 'In-Store Sales', 'Mail-Order Sales'];
  public data1 = [
    [350, 450, 100]
  ];
  public labels2 = ['Ropa', 'Cocina', ' Deporte'];
  public data2 = [
    [25, 100, 50]
  ];

  public labels3 = ['Nike', 'Adidas', ' Puma'];
  public data3 = [
    [456, 345, 12]
  ];

  public labels4 = ['Madera', 'Metal', ' Plástico'];
  public data4 = [
    [123, 34, 75]
  ];
  constructor() { }

  ngOnInit(): void {
  }

}
