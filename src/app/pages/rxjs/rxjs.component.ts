import { Component, OnDestroy } from '@angular/core';
import { Observable, interval, Subscription } from 'rxjs';
import { retry, take, map, filter } from 'rxjs/operators';

@Component({
  selector: 'app-rxjs',
  templateUrl: './rxjs.component.html',
  styleUrls: ['./rxjs.component.css']
})
export class RxjsComponent implements OnDestroy {
  public valorObservable: Subscription;
  constructor() { 

    // this.retornaObservable().pipe(
    //   retry(1)
    // ).subscribe( 
    //   valor => console.log('Subs' + valor),
    //   err => console.warn('Error:', err),
    //   () => console.info('Completo') 
    // );
    this.valorObservable = this.retornaIntervalo()
      .subscribe( (valor) => console.log(valor));
  }

  ngOnDestroy():void {
    this.valorObservable.unsubscribe();
  }

  retornaIntervalo(): Observable<number> {
    const interval$ = interval(1000).pipe(
      // take(10),
      map( valor => {
        return valor +1
      }),
      filter( valor => (valor % 2 === 0) ? true : false ),
    );
    return interval$;
  }

  retornaObservable(): Observable<number> {
    const obs$ = new Observable<number>( observer => {
      let i = 0;
      setInterval( () => {
        i++;
        observer.next(i);
        if(i === 4) {
          observer.complete();
        }

        if(i === 2 ) {
          observer.error('i llego al valor de 2');
        }
      }, 1000)
    });
    return obs$;
  }

}
