import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-promesas',
  templateUrl: './promesas.component.html',
  styles: []
})
export class PromesasComponent implements OnInit {

  constructor() { }

  ngOnInit() {

  //   const promesa = new Promise((resolve, reject) => {
  //     if(false){
  //       resolve('Hola mundo');
  //     } else {
  //       reject('Error');
  //     }
    
  //   });

  //   promesa
  //     .then((mensaje) => {
  //     console.log(mensaje);
  //     })
  //     .catch(err => console.log('Error en mi promesa', err));

  //   console.log('Fin de3 Init'); 
  // this.getUsuarios();
    this.getUsuarios().then(usuarios => console.log(usuarios));
}

  getUsuarios() {
    const promesa = new Promise( resolve => {
    fetch('https://reqres.in/api/users')
      .then((response) => response.json())
      .then(body => resolve(body.data))
    });
    return promesa;
  }

}
